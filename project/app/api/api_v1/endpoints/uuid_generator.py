# -*- coding: utf-8 -*-

from typing import Any, List
from fastapi import status
from fastapi import APIRouter, Body, Depends, HTTPException
from sqlalchemy.orm import Session
from app import models, schemas
from app.utils.service_result import handle_result
from app.services.services.service_uuid import generate_uuid

router = APIRouter()


@router.get("/", name="uuid:get-uuid")
async def get_uuid(size: int = 1) -> Any:
    """
    Generate one or more uuid (max 100)
    """
    uuid_list = generate_uuid(size=size)
    return handle_result(uuid_list)
