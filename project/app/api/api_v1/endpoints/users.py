# -*- coding: utf-8 -*-

from typing import Any, List
from fastapi import status
from fastapi import APIRouter, Body, Depends, HTTPException
from sqlalchemy.orm import Session
from app import models, schemas
from app.services.services.service_user import UserService
from app.api import deps
from app.utils.service_result import handle_result

router = APIRouter()


@router.get("/", name="users:list-users", response_model=List[schemas.User])
async def list_users(
    db: Session = Depends(deps.get_db),
    skip: int = 0,
    limit: int = 100,
) -> Any:
    """
    Retrieve users.
    """
    users = UserService(db).list_users(skip=skip, limit=limit)
    return handle_result(users)


@router.post("/", name="users:create-user", response_model=schemas.User, status_code=status.HTTP_201_CREATED)
async def create_user(
    *,
    db: Session = Depends(deps.get_db),
    user_in: schemas.UserCreate,
) -> Any:
    """
    Create new user.
    """
    result = UserService(db).create_user(user_in)
    return handle_result(result)


@router.get("/{user_id}/", name="users:get-user", response_model=schemas.User)
async def get_user(user_id: str, db: Session = Depends(deps.get_db)) -> Any:
    """
    Get a single user.
    """
    result = UserService(db).get_user(user_id=user_id)
    return handle_result(result)


@router.delete("/{user_id}/", name="users:delete-user", response_model=schemas.User, status_code=status.HTTP_200_OK)
async def delete_user(user_id: str, db: Session = Depends(deps.get_db)) -> Any:
    """
    Delete one user
    """
    result = UserService(db).delete_user(user_id=user_id)
    return handle_result(result)
