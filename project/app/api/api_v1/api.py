# -*- coding: utf-8 -*-

from fastapi import APIRouter
from app.api.api_v1.endpoints import users, login, uuid_generator


api_router = APIRouter()
api_router.include_router(users.router, prefix="/users", tags=["users"])
api_router.include_router(login.router, prefix="/login", tags=["login"])
api_router.include_router(uuid_generator.router, prefix="/uuid", tags=["uuid"])
