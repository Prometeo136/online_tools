# -*- coding: utf-8 -*-

import uuid
from sqlalchemy.orm import Session
from app.services.services.service_user import UserService
from app.models.users import Users
from app.schemas.user import User, UserCreate, UserUpdate
from app.core.security import get_password_hash
from app.tests.factories.user import factory_create_user
from app.utils.service_result import ServiceResult


class TestBasicCrudServices:
    def test_create_user(self, db: Session) -> None:
        email = "test@gmail.com"
        password = "password"
        user_data = UserCreate(email=email, password=password, is_active=True, is_superuser=False)
        user = UserService(db).create_user(obj_in=user_data)
        assert user.success

    def test_create_duplicated_email(self, db: Session) -> None:
        email = "test@gmail.com"
        factory_create_user(db, email=email)
        email = "test@gmail.com"
        password = "password"
        user_data = UserCreate(email=email, password=password, is_active=True, is_superuser=False)
        user = UserService(db).create_user(obj_in=user_data)
        assert not user.success

    def test_get_user_by_id(self, db: Session) -> None:
        user = factory_create_user(db)
        retrieved_user = UserService(db).get_user(user_id=user.id)
        assert retrieved_user.success

    def test_get_user_by_id_incorrect(self, db: Session) -> None:
        factory_create_user(db)
        retrieved_user = UserService(db).get_user(user_id=uuid.uuid4())
        assert not retrieved_user.success

    def test_get_user_by_email(self, db: Session) -> None:
        user = factory_create_user(db)
        retrieved_user = UserService(db).get_user_by_email(email=user.email)
        assert retrieved_user.success

    def test_get_user_by_email_incorrect(self, db: Session) -> None:
        factory_create_user(db)
        retrieved_user = UserService(db).get_user_by_email(email="error@gmail.com")
        assert not retrieved_user.success

    def test_get_list_of_users(self, db: Session) -> None:
        factory_create_user(db, size=10)
        users = UserService(db).list_users()
        assert users.success
        assert len(users.value) == 10

    def test_update_user(self, db: Session) -> None:
        user = factory_create_user(db, is_superuser=True)
        user_data = UserUpdate(is_superuser=False, is_active=False)
        user_updated = UserService(db).update_user(db_obj=user, obj_in=user_data)
        assert user_updated.success
        assert not user_updated.value.is_active
        assert not user_updated.value.is_superuser

    def test_update_user_duplicated_email(self, db: Session) -> None:
        duplicated_email = "test@gmail.com"
        user = factory_create_user(db, is_superuser=True)
        factory_create_user(db, email=duplicated_email)
        user_data = UserUpdate(email=duplicated_email, is_superuser=False, is_active=False)
        user_updated = UserService(db).update_user(db_obj=user, obj_in=user_data)
        assert not user_updated.success

    def test_delete_one_user(self, db: Session) -> None:
        user = factory_create_user(db, is_active=True)
        user_deleted = UserService(db).delete_user(user_id=str(user.id))
        assert user_deleted.success
        assert not user_deleted.value.is_active

    def test_authenticate_user(self, db: Session) -> None:
        password = "test"
        hashed_password = get_password_hash(password)
        user = factory_create_user(db, size=1, hashed_password=hashed_password)
        authenticated_user = UserService(db).authenticate_user(email=user.email, password=password)
        assert authenticated_user.success

    def test_authenticate_user_fails(self, db: Session) -> None:
        password = "test"
        hashed_password = get_password_hash(password)
        user = factory_create_user(db, size=1, hashed_password=hashed_password)
        authenticated_user = UserService(db).authenticate_user(email=user.email, password="error")
        assert not authenticated_user.success
