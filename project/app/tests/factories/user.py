# -*- coding: utf-8 -*-

import random
import uuid
import factory
import factory.fuzzy as fuzzy
from app.core.security import get_password_hash
from app.models import Users


def factory_create_user(session, *, size: int = 1, **kwargs) -> Users:
    UserFactory._meta.sqlalchemy_session = session
    if size > 1:
        return UserFactory.create_batch(size=size)
    return UserFactory.create(**kwargs)


class UserFactory(factory.alchemy.SQLAlchemyModelFactory):
    id = factory.sequence(lambda n: f"{uuid.uuid4()}")
    email = factory.Faker("email")
    is_superuser = random.getrandbits(1)
    hashed_password = get_password_hash("password")
    is_active = True

    class Meta:
        model = Users
        sqlalchemy_session_persistence = "commit"
