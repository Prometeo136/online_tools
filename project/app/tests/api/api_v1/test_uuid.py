# -*- coding: utf-8 -*-

import uuid
from starlette.testclient import TestClient
from starlette.status import HTTP_200_OK, HTTP_400_BAD_REQUEST
from app.main import app


class TestApiUUID:
    def test_generate_one_uuid(self, client: TestClient) -> None:
        response = client.get(app.url_path_for("uuid:get-uuid"))
        assert response.status_code == HTTP_200_OK
        assert len(response.json()) == 1

    def test_generate_many_uuid(self, client: TestClient) -> None:
        url = app.url_path_for("uuid:get-uuid") + "?size=5"
        response = client.get(url)
        assert response.status_code == HTTP_200_OK
        assert len(response.json()) == 5

    def test_exceed_max_uuid_generation(self, client: TestClient) -> None:
        url = app.url_path_for("uuid:get-uuid") + "?size=200"
        response = client.get(url)
        print(response.json())
        assert response.status_code == HTTP_400_BAD_REQUEST
