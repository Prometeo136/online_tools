# -*- coding: utf-8 -*-

import os
import pytest
from sqlalchemy.orm import Session
from starlette.testclient import TestClient
from starlette.status import HTTP_200_OK, HTTP_201_CREATED, HTTP_404_NOT_FOUND, HTTP_422_UNPROCESSABLE_ENTITY
from app.main import app
from app.models.users import Users
from app.schemas import UserCreate
from app.tests.factories.user import factory_create_user


class TestApiUser:
    def test_exists_create_user_api_url(self, client: TestClient) -> None:
        response = client.post(app.url_path_for("users:create-user"), json={})
        assert response.status_code != HTTP_404_NOT_FOUND

    def test_invalid_input_create_user_api(self, client: TestClient) -> None:
        response = client.post(app.url_path_for("users:create-user"), json={})
        assert response.status_code == HTTP_422_UNPROCESSABLE_ENTITY

    def test_create_user_api(self, db: Session, client: TestClient) -> None:
        user_data = {
            "email": "test120@test.com",
            "password": "password",
            "is_active": True,
            "is_superuser": False,
        }
        response = client.post(app.url_path_for("users:create-user"), json=user_data)
        assert response.status_code == HTTP_201_CREATED

    def test_list_users(self, db: Session, client: TestClient) -> None:
        factory_create_user(db, size=10)
        response = client.get(app.url_path_for("users:list-users"))
        assert response.status_code == HTTP_200_OK
        assert len(response.json()) == 10

    def test_retrieve_user(self, db: Session, client: TestClient) -> None:
        user = factory_create_user(db)
        url = app.url_path_for("users:get-user", user_id=str(user.id))
        response = client.get(url)
        assert response.status_code == HTTP_200_OK
        assert response.json().get("id") == str(user.id)

    def test_wrong_id_for_retrieve_user(self, db: Session, client: TestClient) -> None:
        factory_create_user(db)
        url = app.url_path_for("users:get-user", user_id="a7e12eb6-eb52-4516-9ec0-986e26a7a9af")
        response = client.get(url)
        assert response.status_code == HTTP_404_NOT_FOUND

    def test_delete_user(self, db: Session, client: TestClient) -> None:
        user = factory_create_user(db, is_active=True)
        url = app.url_path_for("users:delete-user", user_id=str(user.id))
        response = client.delete(url)
        user_retrieved = db.query(Users).filter(Users.id == str(user.id)).first()
        assert response.status_code == HTTP_200_OK
        assert not user_retrieved.is_active
