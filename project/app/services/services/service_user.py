# -*- coding: utf-8 -*-

from pydantic import EmailStr
from app.models.users import Users
from app.schemas import User, UserCreate, UserUpdate
from app.utils.app_exceptions import AppException
from app.utils.service_result import ServiceResult
from app.services.main import AppService
from app.services.crud import user as crud_user


class UserService(AppService):
    def create_user(self, obj_in: UserCreate) -> ServiceResult:
        user_exists = crud_user.get_by_email(self.db, email=obj_in.email)
        if user_exists:
            return ServiceResult(
                AppException.BaseCreateItem(
                    {
                        "message": "This email is already registered in the database",
                        "email": obj_in.email,
                    }
                )
            )
        user_item = crud_user.create(self.db, obj_in=obj_in)
        if not user_item:
            return ServiceResult(AppException.BaseCreateItem())
        return ServiceResult(user_item)

    def get_user(self, user_id: str) -> ServiceResult:
        user_item = crud_user.get(self.db, id=user_id)
        if not user_item:
            return ServiceResult(AppException.BaseGetItem({"item_id": user_id}))
        return ServiceResult(user_item)

    def get_user_by_email(self, email: EmailStr) -> ServiceResult:
        user_item = crud_user.get_by_email(self.db, email=email)
        if not user_item:
            return ServiceResult(AppException.BaseGetItem({"email": email}))
        return ServiceResult(user_item)

    def list_users(self, *, skip: int = 0, limit: int = 100) -> ServiceResult:
        return ServiceResult(crud_user.get_multi(self.db, skip=skip, limit=limit))

    def update_user(self, db_obj: Users, obj_in: UserUpdate) -> Users:
        user_exists = self.db.query(Users).filter(Users.email == obj_in.email, Users.id != db_obj.id).first()
        if user_exists:
            return ServiceResult(
                AppException.BaseUpdateItem(
                    {
                        "message": "This email is already registered in the database",
                        "email": obj_in.email,
                    }
                )
            )
        user = crud_user.update(self.db, db_obj=db_obj, obj_in=obj_in)
        return ServiceResult(user)

    def delete_user(self, user_id: str) -> Users:
        user = crud_user.delete(self.db, id=user_id)
        return ServiceResult(user)

    def authenticate_user(self, email: EmailStr, password: str) -> Users:
        user = crud_user.authenticate(self.db, email=email, password=password)
        if not user:
            return ServiceResult(AppException.AuthenticationFailure({"message": "Incorrect email or password"}))
        if not user.is_active:
            return ServiceResult(AppException.AuthenticationFailure({"message": "Inactive User"}))
        return ServiceResult(user)
