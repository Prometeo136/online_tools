# -*- coding: utf-8 -*-

import uuid
from app.utils.service_result import ServiceResult
from app.utils.app_exceptions import AppException


def generate_uuid(size: int) -> uuid:
    if size < 101:
        uuid_list = []
        for i in range(size):
            uuid_list.append(str(uuid.uuid4()))
        return ServiceResult(uuid_list)
    else:
        return ServiceResult(AppException.BaseBadRequest({"size": "Exceeded max size (100)"}))
