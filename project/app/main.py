# -*- coding: utf-8 -*-

from fastapi.exceptions import RequestValidationError
from starlette.exceptions import HTTPException as StarletteHTTPException
from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from app.core import tasks
from app.core.config import settings
from app.api.api_v1.api import api_router as api_router_v1
from app.utils.app_exceptions import app_exception_handler, AppExceptionCase
from app.utils.request_exceptions import (
    http_exception_handler,
    request_validation_exception_handler,
)


def get_application():
    app = FastAPI(title=settings.PROJECT_NAME, version=settings.VERSION)

    app.add_middleware(
        CORSMiddleware,
        allow_origins=["*"],
        allow_credentials=True,
        allow_methods=["*"],
        allow_headers=["*"],
    )

    app.add_event_handler("startup", tasks.create_start_app_handler(app))
    app.add_event_handler("shutdown", tasks.create_stop_app_handler(app))

    @app.exception_handler(StarletteHTTPException)
    async def custom_http_exception_handler(request, e):
        return await http_exception_handler(request, e)

    @app.exception_handler(RequestValidationError)
    async def custom_validation_exception_handler(request, e):
        return await request_validation_exception_handler(request, e)

    @app.exception_handler(AppExceptionCase)
    async def custom_app_exception_handler(request, e):
        return await app_exception_handler(request, e)

    app.include_router(api_router_v1, prefix=f"{settings.API_PREFIX}/v1")

    @app.get("/")
    async def root():
        return {"message": "Hello World"}

    return app


app = get_application()
