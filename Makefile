.PHONY: help build up requirements clean lint test help
PROJECT=online_tools
migration_message=''
ROOT_DIR=$(shell pwd)
SOURCE_DIR=$(ROOT_DIR)/
DOCKER_COMPOSE_FILE=docker-compose-local.yaml
POST_MESSAGE=................................................................
pip_install= pip install -r
docker-compose:=docker-compose -f $(DOCKER_COMPOSE_FILE)

help:
	@echo 'Makefile for: ${PROJECT}'
	@echo ''
	@echo '  Options:'
	@echo '    setup                                         set up the project'
	@echo '    start                                         start the project'
	@echo '    stop                                          stop the container(s)'
	@echo '    restart                                       restart the container(s)'
	@echo '    tests                                         run tests'
	@echo '    logs                                          show logs'
	@echo '    migrations migration_message = <message>      make migrations and migrate'
	@echo ''

logs:
	$(docker-compose) logs -f

migrations:
	@echo "Running migrations ${POST_MESSAGE}"
	$(docker-compose) exec fastapi alembic revision --autogenerate -m $(migration_message)
	$(docker-compose) exec fastapi alembic upgrade head

restart:
	@make -s stop
	@make -s start

setup:
	@echo "Building dev stage ${POST_MESSAGE}"
	${pip_install} requirements/dev.txt
	@echo "Installing precommit hooks ${POST_MESSAGE}"
	pre-commit install
	@echo "Building image from docker compose ${POST_MESSAGE}"
	$(docker-compose) build --no-cache

start:
	@echo "Starting the container ${POST_MESSAGE}"
	$(docker-compose) up -d

stop:
	@echo "Stopping the container ${POST_MESSAGE}"
	$(docker-compose) stop

tests:
	@echo "Running tests ${POST_MESSAGE}"
	$(docker-compose) exec fastapi pytest --cov
